<?php

/*
Plugin Name: WP - Map Search
Plugin URI: http://workingmind.net
Author: Ashfaq Ahmed
Author URI: http://workingmind.net
Description: Allows to create search maps where users can search listings using a map. 
Version: 1.0.0 beta
*/

define( 'WP_MAP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

if( ! class_exists( 'Gamajo_Template_Loader' ) ) { 
 	require WP_MAP_PLUGIN_DIR . 'includes/class-gamajo-template-loader.php'; 
} 

require WP_MAP_PLUGIN_DIR . 'includes/wp-map-template-loader.php';

require WP_MAP_PLUGIN_DIR . 'wp_map_functions.php';

class Wp_map_settings {
	
	public $saved_options ;
	static $add_script;
	public function __construct(){
		$this->saved_options = get_option('wp_map_options');
		$this->register_settings_and_fields();
		add_action( 'add_meta_boxes' , array( $this , 'add_meta_box')  );
		add_action( 'save_post' , array( $this , 'save_meta_box')  );
	}
	
	static function add_shortcodes(){
		add_action( 'wp_ajax_get_listings_hook', array(__CLASS__ , 'the_ajax_action_function') );
		add_action( 'wp_ajax_nopriv_get_listings_hook', array(__CLASS__ , 'the_ajax_action_function')  );
		add_shortcode('wp-map-map', array(__CLASS__ , 'wp_map_map_shortcode'));
		add_shortcode('wp-map-listings', array(__CLASS__ , 'wp_map_listings_shortcode'));
		add_shortcode('wp-map-pagination', array(__CLASS__ , 'wp_map_pagination_shortcode'));
		//scripts
		add_action('init', array(__CLASS__, 'wp_map_register_script'));
		add_action('wp_footer', array(__CLASS__, 'wp_map_print_script'));
	}
	
	static function the_ajax_action_function(){
		$options = get_option('wp_map_options');
		$args = array(
			'post_type' => $options['wp_map_post_type'],
			'posts_per_page' => 10,
			'paged' => isset($_GET['page']) ? esc_attr($_GET['page']) : 1,
			'meta_query' => array(
				array(
					'key' => 'wp_map_latitude',
					'value' => strip_tags($_GET['ne_lat']),
					'type' => 'DECIMAL',
					'compare' => '<'
				),
				array(
					'key' => 'wp_map_latitude',
					'value' => strip_tags($_GET['sw_lat']),
					'type' => 'DECIMAL',
					'compare' => '>'
				),
				array(
					'key' => 'wp_map_longitude',
					'value' => strip_tags($_GET['sw_lng']),
					'type' => 'DECIMAL',
					'compare' => '>'
				),
				array(
					'key' => 'wp_map_longitude',
					'value' => strip_tags($_GET['ne_lng']),
					'type' => 'DECIMAL',
					'compare' => '<'
				)
			),
		);
		$all_taxonomies = get_object_taxonomies( $options['wp_map_post_type'], 'names' );
		$tax_args = array(
			'relation' => 'AND'
		);
		foreach($_GET as $key => $param){
			if(in_array($key, $all_taxonomies) && $param != '-any-'){
				$tax_args['tax_query'][] = array(
					'taxonomy' => $key,
					'field'    => 'term_id',
					'terms'    => esc_attr($param)
				);
			}
		}
		$args += $tax_args;        
		$query = new WP_Query( $args);
		if ( $query->have_posts() ) {
			$results = array();
			while ( $query->have_posts() ) {
				$query->the_post();				global $post;
				$results['results'][] = array(                    					'evenodd' => $post->current_post = $query->current_post +1,					
					'infowindow' => wp_map_get_infowindow_template(),
					'content' => wp_map_get_post_template(),
					'latitude' => get_post_meta(get_the_ID() , 'wp_map_latitude'),
					'longitude' => get_post_meta(get_the_ID() , 'wp_map_longitude')
				);
			}
			
			$results['meta']['page'] = isset($_GET['page']) ? (int) esc_attr($_GET['page']) : 1;
			wp_reset_postdata();
			wp_send_json($results);
		}else{
			$results['results'] = array();
			$results['meta']['page'] = isset($_GET['page']) ? (int) esc_attr($_GET['page']) : 1;
			wp_send_json($results);
		}
	}
	
	public function add_menu_page(){
		add_options_page('WP- Map Settings' , 'WP - Map settings' , 'administrator' , __FILE__ , array('Wp_map_settings' , 'display_options_page'));
	}
	
	public function display_options_page(){
		?>
			<div class="wrap">
				<h2>WP - Map Settings</h2>
				<form method="POST" action="options.php" enctype="multipart/form-data">
					<?php settings_fields('wp_map_options'); ?>
					<?php do_settings_sections(__FILE__); ?>
					<p class="submit">
						<input type="submit" name="submit" class="button-primary" value="Save Changes">
					</p>
				</form>
			</div>
		<?php
	}
	
	public function register_settings_and_fields(){
		register_setting('wp_map_options' , 'wp_map_options'); //3rd param optional cb
		add_settings_section('wp_map_main_section' , 'Main Settings' , array($this , 'wp_map_main_section_cb') , __FILE__);
		add_settings_field('wp_map_post_type' , 'Post Type' , array($this , 'wp_map_get_post_types') , __FILE__ , 'wp_map_main_section' );
		add_settings_field('wp_map_initial_lat' , 'Initial Center Lat' , array($this , 'wp_map_lat') , __FILE__ , 'wp_map_main_section' );
		add_settings_field('wp_map_initial_lng' , 'Initial Center Lng' , array($this , 'wp_map_lng') , __FILE__ , 'wp_map_main_section' );
		add_settings_field('wp_map_initial_zoom' , 'Initial Zoom' , array($this , 'wp_map_zoom') , __FILE__ , 'wp_map_main_section' );
	}
	
	public function wp_map_main_section_cb(){
	
	}
	/*
	* Inputs
	*/
	public function wp_map_get_post_types(){
		$post_types = get_post_types( '' , 'objects');
		$output = "<select name='wp_map_options[wp_map_post_type]'>";
		foreach ( $post_types as $post_type ) {
		   $output .= '<option '.($this->saved_options['wp_map_post_type'] == $post_type->name ? 'selected' : '').' value="'.$post_type->name.'">'.$post_type->labels->name.'</option>';
		}
		$output .= '</select><br>';
		$output .= 'Select a post type you wish to use as a location based entity';
		echo $output;
	}
	
	public function wp_map_lat(){
		$output =  "<input type='text' name='wp_map_options[wp_map_initial_lat]' value='{$this->saved_options['wp_map_initial_lat']}'><br>";
		$output .= 'Enter Latitude (Initial map location)';
		echo $output;
	}
	
	public function wp_map_lng(){
		$output =  "<input type='text' name='wp_map_options[wp_map_initial_lng]' value='{$this->saved_options['wp_map_initial_lng']}'><br>";
		$output .= 'Enter Longitude (Initial map location)';
		echo $output;
	}
	
	public function wp_map_zoom(){
		$output = "<select name='wp_map_options[wp_map_initial_zoom]'>";
		for ($i =1; $i < 18; $i++) {
			$output .= "<option ".($this->saved_options['wp_map_initial_zoom'] == $i ? 'selected' : '')." value='{$i}'>{$i}</option>";
		}
		$output .= "</select><br>";
		$output .= "Default: 8";
		echo $output;
	}
	
	/*
	* Meta box
	*/
	public function add_meta_box(){
		if( $this->saved_options['wp_map_post_type'] ){
			add_meta_box(
				'wp_map_location',
				'Location Information',
				array($this , 'add_meta_box_callback'),
				$this->saved_options['wp_map_post_type'],
				'side',
				'high'
			);
		}
	}
	
	public function add_meta_box_callback($post){
		$latitude = get_post_meta($post->ID , 'wp_map_latitude' , true);
		$longitude = get_post_meta($post->ID , 'wp_map_longitude' , true);
		wp_nonce_field( plugin_basename(__FILE__)  , 'wp_map_nonce_field');
		?>
			<p>Enter the location information</p>
			<label style="display:block;" for="latitude">Latitude</label>
			<input type="text" id="wp_map_latitude" name="wp_map_latitude" value="<?php echo esc_attr($latitude); ?>">
			<label style="display:block;" for="longitude">Longitude</label>
			<input type="text" id="wp_map_longitude" name="wp_map_longitude" value="<?php echo esc_attr($longitude); ?>">
		<?php
	}
	
	
	public function save_meta_box($id){
		if( $this->can_save_meta($id , 'wp_map_nonce_field')){
			if( isset( $_POST['wp_map_latitude'] ) && isset( $_POST['wp_map_longitude'] ) ){
				update_post_meta( $id,'wp_map_latitude', strip_tags($_POST['wp_map_latitude'] ));
				update_post_meta( $id,'wp_map_longitude', strip_tags($_POST['wp_map_longitude'] ));
			}
		}
	}
	
	
	public function can_save_meta($post_id , $nonce){
		//is this an autosave ?
		$is_autosave = wp_is_post_autosave( $post_id );
		//Is this a revision?
		$is_revision = wp_is_post_revision( $post_id );
		//Is the nonce valid
		$is_nonce_valid = ( isset($_POST[$nonce]) && wp_verify_nonce( $_POST[$nonce], plugin_basename(__FILE__) ));
		//return true if the user is able to save the value
		return ! ( $is_autosave && $is_revision ) && $is_nonce_valid;
	} 
	
	/*
	* Shortcodes
	*/
	public function wp_map_map_shortcode(){
		self::$add_script = true;
		return '<div style="min-height:300px;" id="ms-map"></div>';
	}
	
	public function wp_map_listings_shortcode(){
		self::$add_script = true;
		return '<div id="ms-listings"></div>';
	}
	
	public function wp_map_pagination_shortcode (){
		self::$add_script = true;
		return '<div id="ms-pagination"></div>';
	}
	/*
	* Scripts
	*/
	static function wp_map_register_script(){
		wp_register_script('wp-map-library', 'http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places', array(), '', true);
		wp_register_script('wp-map-plugin', plugins_url('jquery.mapSearch.js', __FILE__), array('jquery'), '1.0', true);
		wp_register_script('wp-map-script', plugins_url('mapSearch-script.js', __FILE__), array('jquery'), '1.0', true);
		$options = get_option('wp_map_options');
		$datatoBePassed = array(
			'init_lat' => $options['wp_map_initial_lat'],
			'init_lng' => $options['wp_map_initial_lng'],
			'icon' => plugins_url('img/normal.png', __FILE__),
			'highlighted_icon' => plugins_url('img/highlight.png', __FILE__),
			'request_uri' => admin_url( 'admin-ajax.php')
		);
		wp_localize_script( 'wp-map-script', 'wp_map_data', $datatoBePassed );
	}
	
	static function wp_map_print_script(){
		if ( ! self::$add_script )
			return;
		wp_print_scripts('wp-map-library');
		wp_print_scripts('wp-map-plugin');
		wp_print_scripts('wp-map-script');
	}
}

/*
* Custom widgets
*/
class Wp_map_wisgets extends WP_Widget {

	function Wp_map_wisgets() {
		// Instantiate the parent object
		parent::__construct( false, 'Filters Form' );
	}

	function widget( $args, $instance ) {
		// Widget output
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		?>
			<form id="filters">
			<?php foreach($instance['taxonomy_type'] as $taxonomy_type): ?>
				<?php $tax_object = get_taxonomy( $taxonomy_type) ?>
				<?php $terms = get_terms($taxonomy_type , array(
						'hide_empty' => false
				));?>
				<?php if(count($terms) > 0):  ?>
				<label for="<?php print esc_attr($taxonomy_type); ?>"><?php print esc_attr($tax_object->labels->singular_name); ?></label>
				<select class="widefat" style="width:100%; padding:2px;" name="<?php print esc_attr($taxonomy_type); ?>">
						<option value="-any-"><?php print '-Any-'; ?></option>
					<?php foreach($terms as $term): ?>
						<option value="<?php print esc_attr($term->term_id); ?>"><?php print esc_attr($term->name); ?></option>
					<?php endforeach; ?>
				</select>
				<?php endif; ?>
			<?php endforeach; ?>
			</form>
		<?php
		echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance = $old_instance;
		$instance['taxonomy_type'] = ( ! empty( $new_instance['taxonomy_type'] ) ) ? esc_sql( $new_instance['taxonomy_type'] ) : '';
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? esc_attr( $new_instance['title'] ) : '';
		var_dump($new_instance['title']);
		return $instance;
	}

	function form( $instance ) {
		// Output admin widget options form
		if(empty($instance['taxonomy_type'])){
			$instance['taxonomy_type'] = array();
		}
		$options = get_option('wp_map_options');
		$taxonomies = get_object_taxonomies( $options['wp_map_post_type'], 'names' );
		?> 
			<label for="<?php echo $this->get_field_id( 'taxonomy_type' ); ?>"><?php _e( 'Select Taxonomy:' ); ?></label>
			<select class="widefat" multiple id="<?php echo $this->get_field_id( 'taxonomy_type' ); ?>" name="<?php echo $this->get_field_name( 'taxonomy_type' ); ?>[]">
			<?php foreach($taxonomies as $taxonomy): ?>
				<option <?php print (in_array($taxonomy , $instance['taxonomy_type']) ? 'selected' : ''); ?> value="<?php print $taxonomy; ?>"><?php print esc_attr($taxonomy); ?></option>
			<?php endforeach; ?>
			</select>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Widget Title:' ); ?></label>
			<input class="widefat" value="<?php print esc_attr($instance['title']); ?>" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>">
		<?php
	}
}

